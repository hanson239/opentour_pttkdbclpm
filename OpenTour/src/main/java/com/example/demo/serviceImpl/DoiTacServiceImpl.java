package com.example.demo.serviceImpl;

import com.example.demo.model.DoiTac;
import com.example.demo.repository.DoiTacRepo;
import com.example.demo.service.IDoiTacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class DoiTacServiceImpl implements IDoiTacService {

    @Autowired
    private DoiTacRepo doiTacRepo;

    @Value("${perPage}")
    private int perPage;

    @Override
    public Page<DoiTac> findAll(int page) {
        Sort sort = Sort.by("ten");
        Pageable pageable = PageRequest.of(page, perPage, sort);
        return doiTacRepo.findAll(pageable);
    }

    @Override
    public Page<DoiTac> findByName(int page, String name) {
        Sort sort = Sort.by("ten");
        Pageable pageable = PageRequest.of(page, perPage, sort);
        return doiTacRepo.findByTenContains(name, pageable);
    }

    @Override
    public DoiTac findById(int id) {
        return doiTacRepo.findById(id).get();
    }
}
