package com.example.demo.serviceImpl;

import com.example.demo.model.NhanVien;
import com.example.demo.model.upsertModel.UpsertUser;
import com.example.demo.repository.NhanVienRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Service
public class UserDetailsServiceImpl  implements UserDetailsService {

    @Autowired
    private NhanVienRepo nhanVienRepo;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        NhanVien nhanVien = nhanVienRepo.findByUsername(username);
        if (nhanVien == null){
            System.out.println("not found");
            throw new UsernameNotFoundException("User not found");
        }
        else {
            return new UpsertUser(nhanVien);
        }
    }
}
