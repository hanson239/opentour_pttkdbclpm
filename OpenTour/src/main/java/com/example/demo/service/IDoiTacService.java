package com.example.demo.service;

import com.example.demo.model.DoiTac;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface IDoiTacService {
    Page<DoiTac> findAll(int page);

    Page<DoiTac> findByName(int page, String name);

    DoiTac findById(int id);

}
