package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DichVuSuDung implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private TourDi tourSuDung;

    @OneToOne
    private DichVuDoiTac dichVuDoiTac;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime ngaySuDung;


    private double gia;
    private int soLuong;
    private int soLuongNguoiDung;

}
