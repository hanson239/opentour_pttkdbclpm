package com.example.demo.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiaChi implements Serializable {

//    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String soNha;
    private String toaNha;
    private String xom_pho;
    private String phuong_xa;
    private String quan_huyen;
    private String tinh_thanhPho;

    @OneToMany(mappedBy = "diachi")
    private Collection<NhanVien> staffs;

}
