package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HoaDon implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private DoiTac doiTac;

    @OneToMany
    private Collection<DichVuSuDung> dichVuSuDungs;

    @ManyToOne
    private NhanVien nhanVien;

    private String thang;
    private double tongTien;
    private int tinhTrangThanhToan;

}
