package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tour implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany
    private Collection<DichVuDoiTac> dichVuDoiTacs;

    @OneToMany(mappedBy = "tour")
    private Collection<TourDi> tourDis;

    private String noiXuatPhat;
    private String ten;
    private String noiDen;
    private String lichtrinh;
    private String mota;
}
