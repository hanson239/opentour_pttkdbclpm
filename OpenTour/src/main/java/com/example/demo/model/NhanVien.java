package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NhanVien implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private HoTen hoten;

    @ManyToOne
    private DiaChi diachi;

    @OneToMany(mappedBy = "nhanVien")
    private Collection<HoaDon> hoaDons;

    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private String vitri;
    private String role;

    public String getFullname(){
        return this.hoten.getHo() + " " + this.hoten.getDem() + " " + this.hoten.getTen();
    }
}
