package com.example.demo.repository;

import com.example.demo.model.DoiTac;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoiTacRepo extends JpaRepository<DoiTac, Integer> {
    @Override
    Page<DoiTac> findAll(Pageable pageable);

    Page<DoiTac> findByTenContains(String name, Pageable pageable);
}
