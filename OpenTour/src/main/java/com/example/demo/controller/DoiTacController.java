package com.example.demo.controller;

import com.example.demo.model.DoiTac;
import com.example.demo.service.IDoiTacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DoiTacController {

    @Autowired
    IDoiTacService doiTacService;

    @GetMapping("/doi-tac")
    public String findDoiTacByPage(Model model,
                                   @RequestParam(name = "page", defaultValue = "1") Integer page,
                                   @RequestParam(name = "search_query", defaultValue = "") String key) {
        if (page < 1) return "403";

        Page<DoiTac> listDoiTac = null;
        String url = "";

        if (key.equals("")) {
            url = "doi-tac?page=";
            listDoiTac = doiTacService.findAll(page-1);
        } else {
            url = "doi-tac?search_query=" + key + "&page=";
            listDoiTac = doiTacService.findByName(page-1, key);
        }
        model.addAttribute("ListDoiTac", listDoiTac.getContent());
        model.addAttribute("totalPages", listDoiTac.getTotalPages());
        model.addAttribute("current", page);
        model.addAttribute("url", url);
        if (page > listDoiTac.getTotalPages())
            return "403";
        return "danh-sach-doi-tac";
    }

    @GetMapping("/doi-tac/{id}")
    public String findDoiTacById(Model model,
                                 @PathVariable(name = "id") Integer id) {
        DoiTac doiTac = doiTacService.findById(id);
        model.addAttribute("DoiTac", doiTac);
        System.out.println(doiTac);
        return "danh-sach-doi-tac";
    }
}
