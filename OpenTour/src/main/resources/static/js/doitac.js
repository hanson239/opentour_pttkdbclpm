$(document).ready(function () {
    $("#formTimDoiTac").submit(function (event) {
        event.preventDefault();

        const $form = $(this);
        const $inputs = $form.find("input, select, button, textarea");

        $inputs.prop("disabled", true);

        const key = $("#find_key").val();

        if (key == ""){
            $inputs.prop("disabled", false);
        }
        else{
            let url = '/doi-tac?search_query=' + key;
            window.location=url;
        }
        // $.ajax({
        //     url: "/doitac/" +
        // })
    })
})
